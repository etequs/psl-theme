<?php

//$icons = get_transient('icons');

if(!isset($icons) || !$icons || !$icons['logo']) {
    $icons= array(
        'logo' => file_get_contents( $conf['theme_dir'] . $conf['svg_dir'] . 'logo.svg' ),
        'logo_emblem' => file_get_contents( $conf['theme_dir'] . $conf['svg_dir'] . 'logo_emblem.svg' ),
        'facebook' => file_get_contents( $conf['theme_dir'] . $conf['svg_dir'] . 'facebook.svg' ),
        'instagram' => file_get_contents( $conf['theme_dir'] . $conf['svg_dir'] . 'instagram.svg' ),
        'phone' => file_get_contents( $conf['theme_dir'] . $conf['svg_dir'] . 'phone.svg' ),
        'whatsapp' => file_get_contents( $conf['theme_dir'] . $conf['svg_dir'] . 'whatsapp.svg' ),
        'youtube' => file_get_contents( $conf['theme_dir'] . $conf['svg_dir'] . 'youtube.svg' ),
        'close' => file_get_contents( $conf['theme_dir'] . $conf['svg_dir'] . 'close.svg' ),
        'menu' => file_get_contents( $conf['theme_dir'] . $conf['svg_dir'] . 'menu.svg' ),
        'filter' => file_get_contents( $conf['theme_dir'] . $conf['svg_dir'] . 'filter.svg' ),
        'mail' => file_get_contents( $conf['theme_dir'] . $conf['svg_dir'] . 'mail.svg' ),
        'msg' => file_get_contents( $conf['theme_dir'] . $conf['svg_dir'] . 'msg.svg' ),
    );

    set_transient('icons', $icons, HOUR_IN_SECONDS);
}
