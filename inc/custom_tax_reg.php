<?php 

add_action( 'init', 'register_theme_body_type_tax' );
add_action( 'init', 'register_theme_wheel_drive_tax' );
add_action( 'init', 'register_theme_gearbox_tax' );
add_action( 'init', 'register_theme_fuel_tax' );
add_action( 'init', 'register_theme_model_tax' );
add_action( 'init', 'register_theme_make_tax' );

function register_theme_body_type_tax() {
    $labels = array(
        'name'              => _x( 'Body types', 'taxonomy general name' ),
        'singular_name'     => _x( 'Body type', 'taxonomy singular name' ),
        'search_items'      => __( 'Search Body types' ),
        'all_items'         => __( 'All Body types' ),
        'parent_item'       => __( 'Parent Body type' ),
        'parent_item_colon' => __( 'Parent Body type:' ),
        'edit_item'         => __( 'Edit Body type' ),
        'update_item'       => __( 'Update Body type' ),
        'add_new_item'      => __( 'Add New Body type' ),
        'new_item_name'     => __( 'New Body type Name' ),
        'menu_name'         => __( 'Body types' ),
    );
    $args   = array(
        'hierarchical'      => true, // make it hierarchical (like categories)
        'labels'            => $labels,
        'show_ui'           => true,
        'show_admin_column' => true,
        'query_var'         => true,
        'rewrite'           => [ 'slug' => 'body_types' ],
    );

    register_taxonomy( 'body', [ 'car', 'used_car' ], $args );
}

function register_theme_wheel_drive_tax() {
    $labels = array(
        'name'              => _x( 'Wheel drive types', 'taxonomy general name' ),
        'singular_name'     => _x( 'Wheel drive type', 'taxonomy singular name' ),
        'search_items'      => __( 'Search Wheel drive types' ),
        'all_items'         => __( 'All Wheel drive types' ),
        'parent_item'       => __( 'Parent Wheel drive type' ),
        'parent_item_colon' => __( 'Parent Wheel drive type:' ),
        'edit_item'         => __( 'Edit Wheel drive type' ),
        'update_item'       => __( 'Update Wheel drive type' ),
        'add_new_item'      => __( 'Add New Wheel drive type' ),
        'new_item_name'     => __( 'New Wheel drive type Name' ),
        'menu_name'         => __( 'Wheel drive types' ),
    );
    $args   = array(
        'hierarchical'      => true, // make it hierarchical (like categories)
        'labels'            => $labels,
        'show_ui'           => true,
        'show_admin_column' => true,
        'query_var'         => true,
        'rewrite'           => [ 'slug' => 'wheel_drive' ],
    );

    register_taxonomy( 'wheel_drive', [ 'car', 'used_car' ], $args );
}

function register_theme_gearbox_tax() {
    $labels = array(
        'name'              => _x( 'Gearbox types', 'taxonomy general name' ),
        'singular_name'     => _x( 'Gearbox type', 'taxonomy singular name' ),
        'search_items'      => __( 'Search Gearbox types' ),
        'all_items'         => __( 'All Gearbox types' ),
        'parent_item'       => __( 'Parent Gearbox type' ),
        'parent_item_colon' => __( 'Parent Gearbox type:' ),
        'edit_item'         => __( 'Edit Gearbox type' ),
        'update_item'       => __( 'Update Gearbox type' ),
        'add_new_item'      => __( 'Add New Gearbox type' ),
        'new_item_name'     => __( 'New Gearbox type Name' ),
        'menu_name'         => __( 'Gearbox types' ),
    );
    $args   = array(
        'hierarchical'      => true, // make it hierarchical (like categories)
        'labels'            => $labels,
        'show_ui'           => true,
        'show_admin_column' => true,
        'query_var'         => true,
        'rewrite'           => [ 'slug' => 'gearbox' ],
    );

    register_taxonomy( 'gearbox', [ 'car', 'used_car' ], $args );
}

function register_theme_fuel_tax() {
    $labels = array(
        'name'              => _x( 'Fuel types', 'taxonomy general name' ),
        'singular_name'     => _x( 'Fuel type', 'taxonomy singular name' ),
        'search_items'      => __( 'Search Fuel types' ),
        'all_items'         => __( 'All Fuel types' ),
        'parent_item'       => __( 'Parent Fuel type' ),
        'parent_item_colon' => __( 'Parent Fuel type:' ),
        'edit_item'         => __( 'Edit Fuel type' ),
        'update_item'       => __( 'Update Fuel type' ),
        'add_new_item'      => __( 'Add New Fuel type' ),
        'new_item_name'     => __( 'New Fuel type Name' ),
        'menu_name'         => __( 'Fuel types' ),
    );
    $args   = array(
        'hierarchical'      => true, // make it hierarchical (like categories)
        'labels'            => $labels,
        'show_ui'           => true,
        'show_admin_column' => true,
        'query_var'         => true,
        'rewrite'           => [ 'slug' => 'fuel' ],
    );

    register_taxonomy( 'fuel', [ 'car', 'used_car' ], $args );
}

function register_theme_model_tax() {
    $labels = array(
        'name'              => _x( 'Models', 'taxonomy general name' ),
        'singular_name'     => _x( 'Model', 'taxonomy singular name' ),
        'search_items'      => __( 'Search Models' ),
        'all_items'         => __( 'All Models' ),
        'parent_item'       => __( 'Parent Model' ),
        'parent_item_colon' => __( 'Parent Model:' ),
        'edit_item'         => __( 'Edit Model' ),
        'update_item'       => __( 'Update Model' ),
        'add_new_item'      => __( 'Add New Model' ),
        'new_item_name'     => __( 'New Model Name' ),
        'menu_name'         => __( 'Models' ),
    );
    $args   = array(
        'hierarchical'      => true, // make it hierarchical (like categories)
        'labels'            => $labels,
        'show_ui'           => true,
        'show_admin_column' => true,
        'query_var'         => true,
        'rewrite'           => [ 'slug' => 'model' ],
    );

    register_taxonomy( 'model', [ 'used_car' ], $args );
}

function register_theme_make_tax() {
    $labels = array(
        'name'              => _x( 'Makes', 'taxonomy general name' ),
        'singular_name'     => _x( 'Make', 'taxonomy singular name' ),
        'search_items'      => __( 'Search Makes' ),
        'all_items'         => __( 'All Makes' ),
        'parent_item'       => __( 'Parent Make' ),
        'parent_item_colon' => __( 'Parent Make:' ),
        'edit_item'         => __( 'Edit Make' ),
        'update_item'       => __( 'Update Make' ),
        'add_new_item'      => __( 'Add New Make' ),
        'new_item_name'     => __( 'New Make Name' ),
        'menu_name'         => __( 'Marka' ),
    );
    $args   = array(
        'hierarchical'      => true, // make it hierarchical (like categories)
        'labels'            => $labels,
        'show_ui'           => true,
        'show_admin_column' => true,
        'query_var'         => true,
        'rewrite'           => [ 'slug' => 'make' ],
    );

    register_taxonomy( 'make', [ 'car', 'used_car' ], $args );
}