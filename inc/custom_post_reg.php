<?php


function register_theme_custom_post_types() {
    register_post_type( 'car',
        array(
            'labels' => array(
                'name' => __( 'Cars' ),
                'singular_name' => __( 'Car' )
            ),
            'public' => true,
            'show_ui' => true,
            'has_archive' => true,
            'taxonomies' => array('body, wheel_drive, gearbox, fuel'),
            'menu_icon'   => 'dashicons-car',
            'supports' => array('title', 'thumbnail')
        )
    );

    register_post_type( 'used_car',
        array(
            'labels' => array(
                'name' => __( 'Used cars' ),
                'singular_name' => __( 'Used car' )
            ),
            'public' => true,
            'show_ui' => true,
            'has_archive' => true,
            'taxonomies' => array('body, wheel_drive, gearbox, fuel, model'),
            'menu_icon'   => 'dashicons-car',
            'supports' => array('title', 'thumbnail')
        )
    );

    register_post_type( 'review',
        array(
            'labels' => array(
                'name' => __( 'Reviews' ),
                'singular_name' => __( 'Reviews' )
            ),
            'public' => true,
            'show_ui' => true,
            'has_archive' => true,
            'taxonomies' => array(),
            'menu_icon'   => 'dashicons-format-quote'
        )
    );

    register_post_type( 'slide',
        array(
            'labels' => array(
                'name' => __( 'Index slides' ),
                'singular_name' => __( 'Index slide' )
            ),
            'public' => true,
            'show_ui' => true,
            'has_archive' => true,
            'taxonomies' => array(),
            'menu_icon'   => 'dashicons-slides',
            'supports' => array('title', 'thumbnail')
        )
    );

    register_post_type( 'special_offer',
        array(
            'labels' => array(
                'name' => __( 'Special offers' ),
                'singular_name' => __( 'Special offer' )
            ),
            'public' => true,
            'show_ui' => true,
            'has_archive' => true,
            'taxonomies' => array(),
            'menu_icon'   => 'dashicons-star-filled',
            'supports' => array('title', 'editor', 'thumbnail')
        )
    );
}

add_action( 'init', 'register_theme_custom_post_types' );