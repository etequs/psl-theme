<?php 



function get_icon($icon_name, $icon_arr = null, $styles = array() ) {

    if (!$icon_arr) $icon_arr = $icons;

    return sprintf('<div class="icon %s%s">%s</div>',
        $icon_name,
        count($styles) ? ' ' . implode(' ', $styles) : '',
        $icon_arr[$icon_name]
    );
}

// Console
function console_log($output, $with_script_tags = true) {

    $js_code = 'console.log(' . json_encode($output, JSON_HEX_TAG) . ');';

    if ($with_script_tags) {
        $js_code = '<script>' . $js_code . '</script>';
    }

    echo $js_code;

}