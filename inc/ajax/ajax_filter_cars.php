<?php
add_action("wp_ajax_filter_cars", "filter_cars");
add_action("wp_ajax_nopriv_filter_cars", "filter_cars");

function filter_cars(){

    $post_price_field_name = $_POST["post_type"] == 'used_car' ? 'price' : 'minimum_price_per_month';

    $args_avail = array(
        "post_type"         => $_POST["post_type"],
        "post_status"       => "publish",
        "posts_per_page"    => -1
    );

    if ( isset($_POST['filters']) ) {

        $args_avail['tax_query'] = array(
            'relation' => 'AND'
        );

        foreach ( $_POST['filters'] as $key => $value ) {

            if ( $key == 'price' ) continue;

            $tax_filter = array(
                'taxonomy' => $key,
                'field'    => 'term_id',
                'terms'    => $value,
            );
            array_push( $args_avail['tax_query'], $tax_filter );
        }

        if ( isset($_POST['filters']['price']) ) {
            $args_avail['meta_query'] = array(
                array(
                    'key' => $post_price_field_name,
                    'value'   => array( $_POST['filters']['price']['min'], $_POST['filters']['price']['max'] ),
                    'type'    => 'numeric',
                    'compare' => 'BETWEEN',
                )
            );

            
        }
    }

    $filtered_cars = query_posts($args_avail);

    $theme_data = get_fields('47');
    $txt = get_fields( $theme_data['template_text_page'] );

    if ( count( $filtered_cars ) ) {
        foreach ( $filtered_cars as $car ) get_template_part( 'template-parts/car_list/grid_entry', null, array( 'car' => $car, "car_type" => isset($args["car_type"]) ? $args["car_type"] : '', 'txt' => $txt ) );
    } else {
        $options_page = 47;
        $no_cars_found_txt = get_field('no_cars_found_text', $options_page);
        $no_cars_found_txt = $no_cars_found_txt ? $no_cars_found_txt : 'No data';
        echo '<p class="no-cars-found">' . $no_cars_found_txt . '</p>';
    }

    wp_die();

}