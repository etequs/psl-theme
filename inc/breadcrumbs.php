<?php 

function generate_breadcrumbs() {
    $permalink = home_url();
    $breadcrumb = '<a href="' . $permalink . '">PSL</a>';

    if ( get_post_type() == 'page' ) {
        $breadcrumb = $breadcrumb . ' / ' . get_the_title( );
    }

    if ( get_post_type() == 'car' ) {
        $all_cars = get_page(7);
        $permalink = get_permalink(7);
        $breadcrumb = $breadcrumb . ' / <a href="' . $permalink . '">'  . $all_cars->post_title . '</a> / ' . get_the_title( );
    }

    if ( get_post_type() == 'used_car' ) {
        $all_cars = get_page(1151);
        $permalink = get_permalink(1151);
        $breadcrumb = $breadcrumb . ' / <a href="' . $permalink . '">'  . $all_cars->post_title . '</a> / ' . get_the_title( );
    }

    if ( get_post_type() == 'post' ) {
        $all_news = get_page(135);
        $permalink = get_permalink(135);
        $breadcrumb = $breadcrumb . ' / <a href="' . $permalink . '">'  . $all_news->post_title . '</a> / ' . get_the_title( );
    }

    if ( get_post_type() == 'special_offer' ) {
        $all_news = get_page(1209);
        $permalink = get_permalink(1209);
        $breadcrumb = $breadcrumb . ' / <a href="' . $permalink . '">'  . $all_news->post_title . '</a> / ' . get_the_title( );
    }

    return $breadcrumb;
}