<?php

if ( ! defined( '_S_VERSION' ) ) {
	define( '_S_VERSION', '1.0.18' );
}

$theme_data = get_fields('47');

$conf = array(
    'theme_dir' => 'wp-content/themes/psl/',
    'svg_dir' => 'assets/svg/',
    'data' => $theme_data,
    'leasing_guidelines_id' => 63,
    'txt' => get_fields( $theme_data['template_text_page'] )
);

// Register navigation menus
register_nav_menus( array(
    'main-menu' => 'main-menu',
    'footer-menu' => 'footer-menu',
    'footer-menu-left' => 'footer-menu-left',
));

add_filter('use_block_editor_for_post', '__return_false', 10);

function psl_scripts() {
    wp_enqueue_style( 'psl-style', get_stylesheet_uri(), array(), _S_VERSION );
    wp_enqueue_style( 'swiper-css', 'https://cdn.jsdelivr.net/npm/swiper@8/swiper-bundle.min.css', array(), _S_VERSION );
    wp_enqueue_style( 'juqery-ui-css', get_stylesheet_directory_uri() . '/assets/vendor/jquery_ui/jquery-ui.min.css', array(), _S_VERSION );

    wp_enqueue_script( 'juqery-ui-js', get_stylesheet_directory_uri() . '/assets/vendor/jquery_ui/jquery-ui.min.js', array('jquery'), _S_VERSION, true );
    wp_enqueue_script( 'swiper-js', 'https://cdn.jsdelivr.net/npm/swiper@8/swiper-bundle.min.js', array('jquery'), _S_VERSION, true );
    wp_enqueue_script( 'main', get_stylesheet_directory_uri() . '/assets/js/scripts.js', array('jquery', 'juqery-ui-js', 'swiper-js'), _S_VERSION, true );

    wp_localize_script( 'main', 'ajaxLocal', array(
		'ajaxUrl' => admin_url( 'admin-ajax.php' )
	));
}
add_action( 'wp_enqueue_scripts', 'psl_scripts' );

add_theme_support('post-thumbnails');

// variables used in the theme
if (!is_admin()) require_once('inc/vars.php');

// helper functions used in the theme
require_once('inc/theme_functions.php');

// register custom posts needed for theme
require_once('inc/custom_post_reg.php');

// register custom taxonomies needed for theme
require_once('inc/custom_tax_reg.php');

require_once('inc/ajax/ajax_filter_cars.php');

require_once('inc/breadcrumbs.php');

  