<?php

$wheel_drive_data = get_the_terms( $post->ID, 'wheel_drive' );
$gearbox_data = get_the_terms( $post->ID, 'gearbox' );
$fuel_data = get_the_terms( $post->ID, 'fuel' );
$body_type_data = get_the_terms( $post->ID, 'body' );

$body_type = $body_type_data && count($body_type_data) ? $body_type_data[0]->name : '';
$wheel_drive = $wheel_drive_data && count($wheel_drive_data) ? get_term_meta( $wheel_drive_data[0]->term_id, 'display_name', true) : '';
$gearbox = $gearbox_data && count($gearbox_data) ? $gearbox_data[0]->name : '';

$priceField = get_post_type() == 'used_car' ? 'price' : 'minimum_price_per_month';

?>

<div class="overview-control-block">
    <h1 class="car-title"><?php echo $post->post_title; ?></h1>
    <div class="single-page-subtitle">
        <?php /* echo $fuel_data[0]->name; ?>, 
        <?php echo $wheel_drive; ?>, 
        <?php echo $gearbox; ?> <?php _e('ātrumkārba', 'psl'); */ ?>
        <?php echo $body_type; ?>
    </div>
    <p class="line-2 print-only"><?php echo $args["txt"]['from_label']; ?> <span class="large"><?php echo get_post_meta( $post->ID, $priceField, true ); ?> <?php echo $args['txt']['car_list']['after_price_label_for_rent']; ?></span></p>
    <div class="controls">
        <p class="line-1"><?php echo $args["txt"]['apply_and_reserve_now_label']; ?></p>
        <?php if( get_post_type() == 'car' ) : ?>
            <p class="line-2"><?php echo $args["txt"]['from_label']; ?> <span class="large"><?php echo get_post_meta( $post->ID, $priceField, true ); ?> <?php echo $args['txt']['car_list']['after_price_label_for_rent']; ?>*</span></p>
        <?php endif; ?>
        <?php if( get_post_type() == 'used_car' ) : ?>
            <p class="line-2"><span class="large"><?php echo get_post_meta( $post->ID, $priceField, true ); ?> €</span></p>
        <?php endif; ?>
        
        <div class="btn-block">
            <div class="btn highlight-btn" id="reserve-car"><?php echo $args["txt"]['apply_for_consultation']; ?></div>
            <div class="btn" id="to-pdf"><?php echo $args["txt"]['save_pdf']; ?></div>
        </div>
        <p class="disclaimer"><?php /* _e('* Cena ir aprēķināta automašīnai ar lietošanas termiņu 4 gadi (48 mēnesī) un nobraukumu 80 000 km periodā (20 000 km gadā). Pirmā iemaksa - 20% no automašīnas iegādes cenas. Visas cenas norādītas eiro bez PVN (pievienotās vērtības nodokļa)', 'psl'); */ ?></p>
    </div>
</div>