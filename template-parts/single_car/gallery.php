
<?php 
$gallery_data = get_post_meta($post->ID, 'gallery', true);
preg_match('/.*="(.*)"]/', $gallery_data, $id_string);
$media_ids = explode( ',', $id_string[1]);
?>

<div class="gallery">
    <div class="swiper-block-wrapper"></div>
    <div class="main-image-wrapper">
        <div class="swiper main-image">
            <div class="swiper-wrapper">
                <?php
                foreach( $media_ids as $id ) {
                    echo sprintf('<div class="slide swiper-slide" style="background-image:url(%s)" data-id="%s"></div>',
                        wp_get_attachment_image_url( $id, 'large' ),
                        $id
                    );
                }
                ?>
            </div>
            
        </div>
        <div class="nav">
            <div class="arrow arrow-right"></div>
            <div class="arrow arrow-left"></div>
        </div>
        <div class="gallery-counter"><span class="c-item">1</span> / <span class="max-items"><?php echo count($media_ids); ?></span></div>
    </div>
    <div class="gallery-thumbnails-wrapper">
        <div class="swiper gallery-thumbnails">
            <div class="swiper-wrapper">
                <?php
                $row_counter = 0;
                $counter = 0;
                foreach( $media_ids as $id ) {
                    if ( !$row_counter ) echo '<div class="swiper-slide gal-swiper-slide">';
                    echo sprintf('<div class="gal-entry%s" style="background-image:url(%s)" data-id="%s"></div>', !$counter ? ' active' : '', wp_get_attachment_image_url( $id ), $id );
                    $row_counter = !$row_counter ? 1 : 0;
                    $counter++;
                    if (!$row_counter) echo '</div>';
                }

                if ($row_counter) echo '</div>';
                ?>
            </div>
        </div>
    </div>
</div>
