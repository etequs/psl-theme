<?php

$leasing_guidelines = get_post($args['conf']['leasing_guidelines_id']);

?>
<div class="car-details">
    <div class="details-menu">
        <div class="details-menu-entry active"><?php echo $args["txt"]['equipment_label']; ?></div>
        <?php if ( get_post_type() == 'car' ) { ?><div class="details-menu-entry"><?php echo $leasing_guidelines->post_title; ?></div> <?php } ?>
    </div>
    <div class="details-data">
        <div class="details-data-entry active formatted-content" id="equipment">
            <h2 class="print-only"><?php echo $args["txt"]['equipment_label']; ?></h2>
            <div class="inner">
                <?php echo apply_filters( 'the_content', get_post_meta($post->ID, 'equipment', true) ); ?>
            </div>
        </div>
        <?php if ( get_post_type() == 'car' ) : ?>
            <div class="details-data-entry formatted-content" id="leasing-guidelines">
                <h2 class="print-only"><?php echo $leasing_guidelines->post_title; ?></h2>
                <div class="inner">
                    <?php echo apply_filters( 'the_content', $leasing_guidelines->post_content); ?>
                </div>
            </div>
        <?php endif; ?>
    </div>
</div>