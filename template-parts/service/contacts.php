<?php

$options_page = 47;


$service_page_id = get_field('service_page', $options_page);
$contact_data = get_field('service_info', $service_page_id);

?>

<div class="service-contacts-block contacts-page site-center" id="<?php echo $contact_data['service_block_id']; ?>">
    <h2><?php echo $contact_data['title']; ?></h2>
    <p class="before-info"><?php echo $contact_data['address'];?></p>
    

    <div class="cols">
        <div class="left-block">
            <div class="phones">
                <div class="icon-text before-info"><?php echo get_icon('mail', $args['icons'] ); ?><p><?php echo $contact_data['email'];?></p></div>
                <div class="info-block">
                    <h3><?php _e('Autoserviss', 'psl'); ?></h3>
                    <div class="icon-text"><?php echo get_icon('phone', $args['icons'] ); ?><p><?php echo $contact_data['service_phones']; ?></p></div>
                </div>
                <div class="info-block">
                    <h3><?php _e('Rezerves daļas', 'psl'); ?></h3>
                    <div class="icon-text"><?php echo get_icon('phone', $args['icons'] ); ?><p><?php echo $contact_data['reserve_parts_phones']; ?></p></div>
                </div>
            </div>
            <div class="work-hours">
                <h3><?php _e('Darba laiki', 'psl'); ?></h3>
                <p><?php echo apply_filters('the_content', $contact_data['working_hours']); ?></p>
            </div>
        </div>
        <div class="right-block">
            <?php echo $contact_data['map_link']; ?>
        </div>

    </div>
</div>