<?php

$top_banner = get_field('top_banner');
$image = wp_get_attachment_image_url($top_banner['image'], 'full');

?>

<div class="top-image-wrapper" style="background-image:url(<?php echo $image; ?>);">
    <div class="slide-inner site-center">
        <?php echo $top_banner['title']; ?>

        <?php if ( $top_banner['subtitle'] ) : ?>
            <p class="slide-subheading"><?php echo $top_banner['subtitle']; ?></p>
        <?php endif; ?>

        <?php if ( $top_banner['show_button'] ) : ?>
            <?php
                $button_link = '';
                if ( $top_banner['button']['button_link_type'] == 'link' ) $button_link = $top_banner['button']['button_link'];
                if ( $top_banner['button']['button_link_type'] == 'block' ) $button_link = '#' . $top_banner['button']['block_id'];
            ?>
            <div class="slide-button-wrapper">
                <div class="slide-btn">
                    <span class="btn-txt"><?php echo $top_banner['button']['button_title']; ?></span>
                    <a href="<?php echo $button_link; ?>"></a>
                </div>
            </div>
        <?php endif; ?>
        
    </div>
</div>