<?php 

$args = array(
    'post_type' => 'special_offer',
    'posts_per_page' => -1,
    'post_status' => 'publish'
);

$offers = query_posts($args);

?>

<div class="offers-wrapper site-center">

    <h2><?php echo get_field('special_offer_list_title'); ?></h2>

    <div class="offer-list">
        <?php foreach($offers as $offer) : ?>

            <div class="offer-wrapper">
                <div class="offer-entry">
                    <div class="offer-image" style="background-image:url(<?php echo get_the_post_thumbnail_url( $offer->ID ); ?>);"><a href="<?php echo get_permalink($offer->ID); ?>"></a></div>
                    <div class="entry-date"><?php echo get_the_date('d.m.Y', $offer->ID);?></div>
                    <div class="entry-title"><a href="<?php echo get_permalink($offer->ID); ?>"><?php echo $offer->post_title; ?></a></div>
                    <div class="entry-short-details">
                        <?php echo get_the_excerpt($offer->ID); ?>
                    </div>

                </div>
            </div>

        <?php endforeach; ?>
    </div>

</div>