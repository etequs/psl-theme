<?php 
    $data = get_field('service_advantages');
?>

<div class="advantages-wrapper service">
    <div class="site-center">
        <div class="advantages-block block-content">
            <?php foreach ( array(1,2,3,4,5) as $num ) : ?>
                <div class="advantages-entry">
                    <div class="adv-visual">
                        <img src="<?php echo $data['entry'.$num]['icon'] ?>" alt="">
                    </div>
                    <div class="adv-body">
                        <p><?php echo $data['entry'.$num]['description']; ?><p>
                        <span class="price"><?php echo $data['entry'.$num]['price']; ?></span>
                    </div>
                </div>
            <?php endforeach; ?>
            
        </div>
    </div>
</div>