<?php
    $body_types = get_terms( array(
        'taxonomy' => 'body',
        //'hide_empty' => false,
    ) );

    $gearbox_types = get_terms( array(
        'taxonomy' => 'gearbox',
        //'hide_empty' => false,
    ) );

    $makes = get_terms( array(
        'taxonomy' => 'make',
        //'hide_empty' => false,
    ) );

    /* if ( $args["car_type"] == "used" ) {

        $wheel_drive_types = get_terms( array(
            'taxonomy' => 'wheel_drive',
            //'hide_empty' => false,
        ) );

        $gearbox_types = get_terms( array(
            'taxonomy' => 'gearbox',
            //'hide_empty' => false,
        ) );

        $fuel_types = get_terms( array(
            'taxonomy' => 'fuel',
            //'hide_empty' => false,
        ) );

        $model_types = get_terms( array(
            'taxonomy' => 'model',
        ) );

    } */

    $post_type = $args["car_type"] == "used" ? 'used_car' : 'car';
    /* $post_price_field_name = $args["car_type"] == "used" ? 'price' : 'minimum_price_per_month';

    $min_price = $wpdb->get_var('SELECT MIN(meta_value) AS min FROM ' . $wpdb->prefix . 'postmeta AS pm, ' . $wpdb->prefix . 'posts AS po WHERE pm.post_id = po.ID AND po.post_status = "publish" AND po.post_type = "' .  $post_type . '" AND pm.meta_key = "' . $post_price_field_name . '"');
    if (!$min_price) $min_price = 0;
    $max_price = $wpdb->get_var('SELECT MAX(meta_value) AS min FROM ' . $wpdb->prefix . 'postmeta AS pm, ' . $wpdb->prefix . 'posts AS po WHERE pm.post_id = po.ID AND po.post_status = "publish" AND po.post_type = "' .  $post_type . '" AND pm.meta_key = "' . $post_price_field_name . '"');
    if (!$max_price) $max_price = 9999; */
?>

<div class="sidebar filters" data-cartype="<?php echo $args["car_type"]; ?>">
    <div class="filter-header">
        <?php echo get_icon('filter', $args['icons']); ?>
        <?php echo $args["txt"]['filter_heading']; ?>
    </div>

    <div class="filter-block">
        <div class="filter-block-header"><?php echo $args["txt"]['body_type_label']; ?></div>
        <div class="filter-body" data-type="body">
            <?php
            foreach ($body_types as $body_type) {
                echo sprintf('<div class="filter-entry" data-id="%s">%s</div>', $body_type->term_id, $body_type->name);
            }
            ?>
        </div>
    </div>
    <div class="filter-block">
        <div class="filter-block-header"><?php echo $args["txt"]['gearbox_label']; ?></div>
        <div class="filter-body" data-type="gearbox">
            <?php
            foreach ($gearbox_types as $gearbox_type) {
                echo sprintf('<div class="filter-entry" data-id="%s">%s</div>', $gearbox_type->term_id, $gearbox_type->name);
            }
            ?>
        </div>
    </div>

    <div class="filter-block">
        <div class="filter-block-header"><?php echo $args["txt"]['make_label']; ?></div>
        <div class="filter-body" data-type="make">
            <?php
            foreach ($makes as $make) {
                echo sprintf('<div class="filter-entry" data-id="%s">%s</div>', $make->term_id, $make->name);
            }
            ?>
        </div>
    </div>

    <!-- <?php if ( $args["car_type"] == "used" ) : ?>

        <div class="filter-block">
            <div class="filter-block-header"><?php echo $args["txt"]['model_label']; ?></div>
            <div class="filter-body" data-type="model">
                <?php
                foreach ($model_types as $model_type) {
                    echo sprintf('<div class="filter-entry" data-id="%s">%s</div>', $model_type->term_id, $model_type->name);
                }
                ?>
            </div>
        </div>

        <div class="filter-block">
            <div class="filter-block-header"><?php echo $args["txt"]['wheel_drive_label']; ?></div>
            <div class="filter-body" data-type="wheel_drive">
                <?php
                foreach ($wheel_drive_types as $wheel_drive_type) {
                    echo sprintf('<div class="filter-entry" data-id="%s">%s</div>', $wheel_drive_type->term_id, $wheel_drive_type->name);
                }
                ?>
            </div>
        </div>

        <div class="filter-block">
            <div class="filter-block-header"><?php echo $args["txt"]['gearbox_label']; ?></div>
            <div class="filter-body" data-type="gearbox">
                <?php
                foreach ($gearbox_types as $gearbox_type) {
                    echo sprintf('<div class="filter-entry" data-id="%s">%s</div>', $gearbox_type->term_id, $gearbox_type->name);
                }
                ?>
            </div>
        </div>

        <div class="filter-block">
            <div class="filter-block-header"><?php echo $args["txt"]['fuel_type_label']; ?></div>
            <div class="filter-body" data-type="fuel">
                <?php
                foreach ($fuel_types as $fuel_type) {
                    echo sprintf('<div class="filter-entry" data-id="%s">%s</div>', $fuel_type->term_id, $fuel_type->name);
                }
                ?>
            </div>
        </div>

        
    
    <?php endif; ?> 
    
    <?php if ( $args["car_type"] == "new" ) : ?>
        <div class="filter-block">
            <div class="filter-block-header"><?php _e('Cena', 'psl'); ?></div>
            <div class="filter-body">
                <div class="actual-range"><?php echo $min_price; ?> - <?php echo $max_price; ?> Eur</div>
                <div type="range" data-min="<?php echo $min_price; ?>" data-max="<?php echo $max_price; ?>" class="slider" id="price-slide"></div>
                <div class="range-peak-values">
                    <div class="min-peak-val"><?php echo $min_price; ?> Eur</div>
                    <div class="max-peak-val"><?php echo $max_price; ?> Eur</div>
                </div>
            </div>
        </div>
    <?php endif; ?>  -->
</div>