

<div class="page-layout">
    <div class="site-center">
        <div class="breadcrumbs"><?php echo generate_breadcrumbs(); ?></div>
    </div>

    <div class="site-center flex-content">
        <?php
        get_template_part( 'template-parts/car_list/filters', null, array( 'icons' => $args['icons'], "car_type" => $args["car_type"], 'txt' => $args["txt"] ) );
        get_template_part( 'template-parts/car_list/results', null, array( "car_type" => $args["car_type"], 'txt' => $args["txt"] ) );
        ?>
    </div>



