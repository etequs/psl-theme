<?php

$post_type = $args["car_type"] == "new" ? 'car' : "used_car";

$query_args = array(
    "post_type"         => $post_type,
    "post_status"       => "publish",
    "posts_per_page"    => -1
);

$filtered_cars = query_posts($query_args);
?>

<div class="filter-results">
    <?php
    if ( count( $filtered_cars ) ) {
        foreach ( $filtered_cars as $car ) get_template_part( 'template-parts/car_list/grid_entry', null, array( 'car' => $car, "car_type" => isset($args["car_type"]) ? $args["car_type"] : '', 'txt' => $args['txt'] ) );
    } else {
        $options_page = 47;
        $no_cars_found_txt = get_field('no_cars_found_text', $options_page);
        echo '<div class="no-cars-found">' . $no_cars_found_txt ? $no_cars_found_txt : 'No data' . '</div>';
    }
    ?>
    
</div>