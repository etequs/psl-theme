<?php 
$car = $args['car'];
$body_type_data = get_the_terms( $car->ID, 'body' );

$body_type = $body_type_data && count($body_type_data) ? $body_type_data[0]->name : '';


$thumb = get_the_post_thumbnail_url( $car->ID, 'large' );
if (!$thumb) {
    $gallery_data = get_post_meta($car->ID, 'gallery', true);
    if (!$gallery_data) {
        $thumb = ''; 
    } else {
        preg_match('/.*="(.*)"]/', $gallery_data, $id_string);
        $media_ids = explode( ',', $id_string[1]);
        $thumb = wp_get_attachment_image_url( $media_ids[0], 'large' ); 
    }
}

$post_price_field_name = $car->post_type == "used_car" ? 'price' : 'minimum_price_per_month';

$price = get_post_meta( $car->ID, $post_price_field_name, true );
if ( !$price ) $price = 1;

?>

<div class="car-entry-wrapper">
    <div class="car-entry">
        <a href="<?php echo get_permalink( $car ); ?>"></a>
        <div class="car-img" style="background-image: url(<?php echo $thumb; ?>);">
            <?php
                $grid_label = get_post_meta( $car->ID, 'grid_label', true );
                $grid_label_background_color = get_post_meta( $car->ID, 'grid_label_background_color', true );
                $grid_label_text_color = get_post_meta( $car->ID, 'grid_label_text_color', true );

                if ($grid_label_background_color || $grid_label_text_color) {
                    $grid_label_style = sprintf(' style="%s%s"',
                        $grid_label_background_color ? 'background-color:'.$grid_label_background_color.';' : '',
                        $grid_label_background_color ? 'color:'.$grid_label_text_color.';' : ''
                    );
                } else {
                    $grid_label_style = '';
                }

                if ( $grid_label ) {
                    echo sprintf('<div class="grid-label"%s>%s</div>',
                        $grid_label_style ? $grid_label_style : '',
                        $grid_label
                    );
                    //echo '<div class="grid-label">' . $grid_label . '</div>';
                }
            ?>
        </div>
        <div class="car-grid-controls">
            <?php if ($car->post_type == "car") : ?>
                <a href="<?php echo get_permalink( $car ); ?>"></a>
                <div class="car-price-from control"><?php echo $args["txt"]['from_label']; ?> <?php echo $price; ?> <?php echo $args['txt']['car_list']['after_price_label_for_rent']; ?></div>
            <?php endif; ?>
            <?php if ($car->post_type == "used_car") : ?>
                <a href="<?php echo get_permalink( $car ); ?>"></a>
                <div class="car-price-from control"><?php echo $price; ?> <?php echo $args['txt']['car_list']['after_price_label_for_purchase']; ?></div>
            <?php endif; ?>
            <div class="go-to-car control">
                <a href="<?php echo get_permalink( $car ); ?>"></a>
                <span><?php echo $args["txt"]['see_more_label']; ?></span>
            </div>
        </div>
        <div class="entry-info">
            <h3 class="car-title"><?php echo $car->post_title; ?></h3>
            <div class="car-desc">
                <?php echo $body_type; ?>
            </div>
        </div>
    </div>
</div>