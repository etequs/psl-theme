<div class="overlay-wrapper" id="<?php echo $args['id']; ?>">
    <div class="overlay-window">
        <div class="close-overlay"><?php echo get_icon( 'close', $args['icons'] ); ?></div>
        <div class="overlay-body">
            <?php
            if ($args['type'] == 'form') {
                echo do_shortcode($args['shortcode']);
            }
            ?>
        </div>
    </div>
</div>