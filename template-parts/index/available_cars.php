<?php

$args_avail = array(
    "post_type"         => isset($args['car_type']) ? $args['car_type'] : 'car',
    "post_status"       => "publish",
    "posts_per_page"    => -1
);
$available_cars = query_posts($args_avail);

?>

<div class="available-wrapper block-wrapper">
    <div class="site-center">
        <div class="available-block block-content">
            <h2>
                <?php 
                    if (isset($args['car_type']) && $args['car_type'] == 'used_car' ) {
                        echo $args['txt']['car_list']['available_cars_heading'];
                    } else {
                        echo $args['txt']['car_list']['other_cars_heading'];
                    }
                    
                ?>
            </h2>
            <div class="available-car-swiper-wrapper">
                <div class="available-car-swiper swiper">
                    <div class="swiper-wrapper">

                    <?php foreach($available_cars as $car) : ?>
                        <?php
                            $wheel_drive_data = get_the_terms( $car->ID, 'wheel_drive' );
                            $gearbox_data = get_the_terms( $car->ID, 'gearbox' );
                            $fuel_data = get_the_terms( $car->ID, 'fuel' );
                            $body_type_data = get_the_terms( $car->ID, 'body' );

                            $body_type = $body_type_data && count($body_type_data) ? $body_type_data[0]->name : '';

                            $post_price_field_name = $car->post_type == "used_car" ? 'price' : 'minimum_price_per_month';

                            $price = get_post_meta( $car->ID, $post_price_field_name, true );
                            if ( !$price ) $price = 1;

                            //var_dump()

                            $fuel = $fuel_data && count($fuel_data) ? ' ' . get_term_meta( $fuel_data[0]->term_id, 'display_name', true) : '';
                            $wheel_drive = $wheel_drive_data && count($wheel_drive_data) ? get_term_meta( $wheel_drive_data[0]->term_id, 'display_name', true) : '';
                            $gearbox = $gearbox_data && count($gearbox_data) ? $gearbox_data[0]->name : '';
                            $engine_size = get_post_meta($car->ID, 'engine_size', true); 

                            $thumb = get_the_post_thumbnail_url( $car->ID, 'large' );
                            if (!$thumb) {
                                $gallery_data = get_post_meta($car->ID, 'gallery', true);
                                if (!$gallery_data) {
                                    $thumb = ''; 
                                } else {
                                    preg_match('/.*="(.*)"]/', $gallery_data, $id_string);
                                    $media_ids = explode( ',', $id_string[1]);
                                    $thumb = wp_get_attachment_image_url( $media_ids[0], 'large' ); 
                                }
                            }

                        ?>
                        <div class="car-entry swiper-slide">
                            <div class="car-img" style="background-image: url(<?php echo $thumb; ?>);">
                            <?php
                                $grid_label = get_post_meta( $car->ID, 'grid_label', true );
                                $grid_label_background_color = get_post_meta( $car->ID, 'grid_label_background_color', true );
                                $grid_label_text_color = get_post_meta( $car->ID, 'grid_label_text_color', true );
                
                                if ($grid_label_background_color || $grid_label_text_color) {
                                    $grid_label_style = sprintf(' style="%s%s"',
                                        $grid_label_background_color ? 'background-color:'.$grid_label_background_color.';' : '',
                                        $grid_label_background_color ? 'color:'.$grid_label_text_color.';' : ''
                                    );
                                } else {
                                    $grid_label_style = '';
                                }
                
                                if ( $grid_label ) {
                                    echo sprintf('<div class="grid-label"%s>%s</div>',
                                        $grid_label_style ? $grid_label_style : '',
                                        $grid_label
                                    );
                                    //echo '<div class="grid-label">' . $grid_label . '</div>';
                                }
                            ?>
                            </div>
                            <div class="car-info">
                                <h3 class="car-title"><?php echo $car->post_title; ?></h3>
                                <?php /*if ( ($engine_size && $fuel) || $wheel_drive || $gearbox ) : ?>
                                <div class="car-desc">
                                    <?php echo $engine_size; ?><?php echo $fuel; ?>,
                                    <?php echo $wheel_drive; ?>,
                                    <?php echo $gearbox; ?>
                                </div>
                                <?php endif;*/ ?>
                                <?php if ($body_type) : ?>
                                    <div class="car-desc"><?php echo $body_type; ?></div>
                                <?php endif; ?>
                            </div>
                            <div class="car-price">
                                <?php if ($car->post_type == "car") : ?>
                                    <?php $args["txt"]['from_label']; ?> <?php echo $price; ?> <?php echo $args['txt']['car_list']['after_price_label_for_rent']; ?> 
                                <?php endif; ?>
                                <?php if ($car->post_type == "used_car") : ?>a
                                    <?php echo $price; ?> <?php echo $args['txt']['car_list']['after_price_label_for_purchase']; ?>
                                <?php endif; ?>
                            </div>
                            <a href="<?php echo get_permalink( $car ); ?>"></a>
                        </div>
                    <?php endforeach; ?>

                    </div>
                    
                </div>
                <div class="arrow arrow-right"></div>
                <div class="arrow arrow-left"></div>
            </div>
        </div>
    </div>
</div>