<div class="advantages-wrapper">
    <div class="site-center">
        <div class="advantages-block block-content">
            <div class="advantages-entry">
                <div class="adv-visual">
                    <img src="<?php echo get_stylesheet_directory_uri(  ); ?>/assets/tmp/ico1.png" alt="">
                </div>
                <div class="adv-body"><?php echo $args['txt']['advantages']['adv1']; ?></div>
            </div>
            <div class="advantages-entry">
                <div class="adv-visual">
                    <img src="<?php echo get_stylesheet_directory_uri(  ); ?>/assets/tmp/ico2.png" alt="">
                </div>
                <div class="adv-body"><?php echo $args['txt']['advantages']['adv2']; ?></div>
            </div>
            <div class="advantages-entry">
                <div class="adv-visual">
                    <img src="<?php echo get_stylesheet_directory_uri(  ); ?>/assets/tmp/ico3.png" alt="">
                </div>
                <div class="adv-body"><?php echo $args['txt']['advantages']['adv3']; ?></div>
            </div>
            <div class="advantages-entry">
                <div class="adv-visual">
                    <img src="<?php echo get_stylesheet_directory_uri(  ); ?>/assets/tmp/ico4.png" alt="">
                </div>
                <div class="adv-body"><?php echo $args['txt']['advantages']['adv4']; ?></div>
            </div>
            <div class="advantages-entry">
                <div class="adv-visual">
                    <img src="<?php echo get_stylesheet_directory_uri(  ); ?>/assets/tmp/ico5.png" alt="">
                </div>
                <div class="adv-body"><?php echo $args['txt']['advantages']['adv5']; ?></div>
            </div>
        </div>
    </div>
</div>