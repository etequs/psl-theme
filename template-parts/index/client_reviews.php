<?php

$query_args = array(
    "post_type"         => "review",
    "post_status"       => "publish",
    "posts_per_page"    => -1
);
$reviews = query_posts($query_args);

?>

<div class="reviews-wrapper block-wrapper">
    <div class="site-center">
        <div class="reviews-block block-content">
            <h2><?php echo $args['txt']['client_reviews_heading']; ?></h2>
            <div class="review-swiper swiper">
                <div class="swiper-wrapper">
                    <?php foreach ($reviews as $review) : ?>
                    <div class="swiper-slide review-entry-wrapper">
                        <div class="review-entry">
                            <div class="review-body"><?php echo apply_filters( 'the_content', $review->post_content ); ?></div>
                            <div class="review-author"><?php echo $review->post_title; ?></div>
                        </div>
                    </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </div>
</div>