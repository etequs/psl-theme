<?php

$args = array(
    "post_type"         => "slide",
    "post_status"       => "publish",
    "posts_per_page"    => -1
);

$slides = query_posts($args);
$counter = 0;

?>

<div class="fp-slider">
    <?php foreach( $slides as $slide ) : ?>
    <div class="fp-slide<?php echo !$counter ? ' active' : ''; ?>" style="background-image: url( <?php echo get_the_post_thumbnail_url( $slide->ID, 'full' ); ?> );">
        <div class="slide-inner site-center">
            <?php $slide_data = get_fields($slide->ID); ?>
            <?php echo $slide->post_title; ?>

            <?php if ($slide_data['subheading']) : ?>
                <p class="slide-subheading"><?php echo $slide_data['subheading']; ?></p>
            <?php endif; ?>

            <?php if ($slide_data['show_button']) : ?>
                <div class="slide-button-wrapper">
                    <div class="slide-btn">
                        <span class="btn-txt"><?php echo $slide_data['button_text']; ?></span>
                        <a href="<?php echo $slide_data['button_link']; ?>"></a>
                    </div>
                </div>
            <?php endif; ?>
            
        </div>
    </div>
    <?php
    $counter++;
    endforeach;
    ?>
</div>