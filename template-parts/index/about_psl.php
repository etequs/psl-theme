<?php 
    $data = get_fields( 81 );
?>

<div class="about-wrapper block-wrapper" style="background-image: url(<?php echo $data['block_background'] ?>);">
    <div class="site-center">
        <div class="about-block block-content">
            <h2><?php echo $args['txt']['about_block_heading']; ?></h2>
            <div class="about-steps">
                <div class="numbers">
                    <div class="num active" data-num="1">
                        <span>1.</span>
                        <div class="num-desc-entry active" data-num="1"><?php echo $data['step_1']; ?></div>
                    </div>
                    <div class="num" data-num="2">
                        <span>2.</span>
                        <div class="num-desc-entry" data-num="2"><?php echo $data['step_2']; ?></div>
                    </div>
                    <div class="num" data-num="3">
                        <span>3.</span>
                        <div class="num-desc-entry" data-num="2"><?php echo $data['step_3']; ?></div>
                    </div>
                    <div class="num" data-num="4">
                        <span>4.</span>
                        <div class="num-desc-entry center" data-num="2"><?php echo $data['step_4']; ?></div>
                    </div>
                    <div class="num" data-num="5">
                        <span>5.</span>
                        <div class="num-desc-entry right" data-num="2"><?php echo $data['step_5']; ?></div>
                    </div>
                    <div class="num" data-num="6">
                        <span>6.</span>
                        <div class="num-desc-entry right" data-num="2"><?php echo $data['step_6']; ?></div>
                    </div>
                    <div class="num" data-num="7">
                        <span>7.</span>
                        <div class="num-desc-entry right" data-num="2"><?php echo $data['step_7']; ?></div>
                    </div>
                </div>
                <div class="num-descriptions">
                    <div class="num-desc-entry active" data-num="1"><span class="alt-num">1. </span><span><?php echo $data['step_1']; ?></span></div>
                    <div class="num-desc-entry" data-num="2"><span class="alt-num">2. </span><span><?php echo $data['step_2']; ?></span></div>
                    <div class="num-desc-entry" data-num="3"><span class="alt-num">3. </span><span><?php echo $data['step_3']; ?></span></div>
                    <div class="num-desc-entry" data-num="4"><span class="alt-num">4. </span><span><?php echo $data['step_4']; ?></span></div>
                    <div class="num-desc-entry" data-num="5"><span class="alt-num">5. </span><span><?php echo $data['step_5']; ?></span></div>
                    <div class="num-desc-entry" data-num="6"><span class="alt-num">6. </span><span><?php echo $data['step_6']; ?></span></div>
                    <div class="num-desc-entry" data-num="7"><span class="alt-num">7. </span><span><?php echo $data['step_7']; ?></span></div>
                </div>
            </div>
        </div>
    </div>
</div>