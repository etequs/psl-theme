<?php get_header( null, array('icons' => $icons, 'conf' => $conf['data']) ); ?>

<div class="page-layout">
    <div class="site-center">
        <div class="breadcrumbs"><?php echo generate_breadcrumbs(); ?></div>
    </div>

    <div class="site-center print-only">
        <?php
            $gallery_data = get_post_meta($post->ID, 'gallery', true);
            preg_match('/.*="(.*)"]/', $gallery_data, $id_string);
            $media_ids = explode( ',', $id_string[1]);
            $url = wp_get_attachment_image_url( $media_ids[0], 'large' );
        ?>
        <div class="main-print-image">
            <img src="<?php echo $url; ?>">
        </div>
    </div>

    <div class="site-center ">
        <div class="car-overview flex-content">
            <?php get_template_part( 'template-parts/single_car/overview_controls', null, array( 'txt' => $conf['txt'] ) ); ?>
            <?php get_template_part( 'template-parts/single_car/gallery' ); ?>
        </div>
        <div class="car-details">
            <?php get_template_part( 'template-parts/single_car/details', null, array( "conf" => $conf, 'txt' => $conf['txt'] ) ); ?>
        </div>
    </div>
</div>

<?php get_template_part( 'template-parts/overlays/overlay-container', null, array("icons" => $icons, 'id' => 'reserve-car-overlay', 'type' => 'form', 'shortcode' => '[contact-form-7 id="77" title="Reserve"]') ); ?>
<?php get_template_part( 'template-parts/overlays/overlay-container', null, array("icons" => $icons, 'id' => 'more-info-overlay', 'type' => 'form', 'shortcode' => '[contact-form-7 id="78" title="Get more info"]') ); ?>

<?php get_template_part( 'template-parts/index/available_cars', null, array('car_type' => 'car') ); ?>

<?php get_footer( null, array('icons' => $icons, 'conf' => $conf['data'], 'txt' => $conf['txt'] )); ?>