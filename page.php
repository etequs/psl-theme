<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package psl
 */

 get_header( null, array('icons' => $icons, 'conf' => $conf['data']) );
?>

	<main class="site-main page-layout site-center">

        <div class="breadcrumbs"><?php echo generate_breadcrumbs(); ?></div>

        <?php
            $page_img = get_the_post_thumbnail_url();
            if ( $page_img ) :
        ?>
        
        <div class="main-page-image" style="background-image:url(<?php echo get_the_post_thumbnail_url(); ?>)"></div>

        <?php endif; ?>

        <div class="formatted-content">
            <?php
            while ( have_posts() ) :
                the_post();
                echo '<h1>' . $post->post_title . '</h1>';
                echo apply_filters( 'the_content', $post->post_content );

            endwhile; // End of the loop.
            ?>
        </div>

	</main><!-- #main -->

<?php
get_footer( null, array('icons' => $icons, 'conf' => $conf['data'], 'txt' => $conf['txt'] ));
