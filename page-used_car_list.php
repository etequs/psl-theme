<?php /* Template Name: Used car list */ ?>

<?php 

get_header( null, array('icons' => $icons, 'conf' => $conf['data'], 'txt' => $conf['txt']) );

get_template_part( 'template-parts/car_list/layout', null, array('icons' => $icons, "car_type" => "used", 'txt' => $conf['txt'] ) );

get_footer( null, array('icons' => $icons, 'conf' => $conf['data'], 'txt' => $conf['txt'] ));
