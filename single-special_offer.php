<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package psl
 */

get_header( null, array('icons' => $icons, 'conf' => $conf['data']) );

//$title = $post->post_type == 'post' ? __('Jaunumi', 'psl') : $post->post_title;
$title = $post->post_title;

?>

    <main class="site-main page-layout">

        <div class="site-center">
            <div class="breadcrumbs"><?php echo generate_breadcrumbs(); ?></div>
            <h1><?php echo $title; ?> </h1>
            <?php
            while ( have_posts() ) :
                the_post();?>

                <?php $form = get_fields(); ?>

                <div class="single-post-content flex-single-post">
                    <div class="single-post-image">
                        <img src="<?php echo get_the_post_thumbnail_url( $post->ID ); ?>" alt="">
                    </div>
                    <div class="single-post-content formatted-content">
                        <?php echo apply_filters('the_content', $post->post_content); ?>
                    </div>
                </div>

                <?php if ($form) : ?>
                    <div class="service-form site-center" id="<?php echo $form['contact_form_id']; ?>">
                        <h2><?php echo $form['title'];?></h2>
                        <p class="subtitle"><?php echo $form['subtitle']; ?></p>
                        <div class="contact-form-wrapper">
                            <?php echo do_shortcode( $form['form_shortcode'] ); ?>
                        </div>
                    </div>
                <?php endif ;?>
                

            <?php 
            endwhile; // End of the loop.
            ?>
        </div>

	</main><!-- #main -->

<?php
get_footer( null, array('icons' => $icons, 'conf' => $conf['data'], 'txt' => $conf['txt'] ));
