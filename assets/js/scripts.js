/* eslint-disable linebreak-style */
/* eslint-disable no-trailing-spaces */
/* eslint-disable padded-blocks */
/* eslint-disable indent */
/* eslint-disable no-console */

/*global jQuery, Swiper, ajaxLocal */
jQuery( 'document' ).ready( function( $ ) {
    let minPriceVal = $( '#price-slide' ).data( 'min' ) ? $( '#price-slide' ).data( 'min' ) : 0;
    let maxPriceVal = $( '#price-slide' ).data( 'max' ) ? $( '#price-slide' ).data( 'max' ) : 9999;

    $( '#price-slide' ).slider( {
        range: true,
        min: $( '#price-slide' ).data( 'min' ) ? $( '#price-slide' ).data( 'min' ) : 0,
        max: $( '#price-slide' ).data( 'max' ) ? $( '#price-slide' ).data( 'max' ) : 9999,
        values: [ $( '#price-slide' ).data( 'min' ) ? $( '#price-slide' ).data( 'min' ) : 0, $( '#price-slide' ).data( 'max' ) ? $( '#price-slide' ).data( 'max' ) : 9999 ],
        slide: ( event, ui ) => {
            $( '.actual-range' ).text( ui.values[ 0 ] + ' - ' + ui.values[ 1 ] + ' Eur' );
            minPriceVal = ui.values[ 0 ];
            maxPriceVal = ui.values[ 1 ];
            filterEntries();
        },
    } );

    console.log( minPriceVal, maxPriceVal );

    if ( $( '.fp-slide' ).length > 1 ) {
        setInterval( () => {

            const nowActiveSlide = $( '.fp-slide.active' );
            
            if ( nowActiveSlide.next().length ) {
                nowActiveSlide.next().addClass( 'active' );
            } else {
                $( '.fp-slide' ).eq( 0 ).addClass( 'active' );
            }

            nowActiveSlide.removeClass( 'active' );

        }, 2000 ); 
    }

    $( '.num' ).click( function() {
        
        if ( $( this ).hasClass( 'active' ) ) {
            return;
        }

        const theNum = parseInt( $( this ).data( 'num' ) );

        if ( theNum < 4 ) {
            $( '.num-descriptions' ).removeClass( 'is-right' );
            $( '.num-descriptions' ).removeClass( 'is-center' );
        } else if ( theNum === 4 ) {
            $( '.num-descriptions' ).removeClass( 'is-right' );
            $( '.num-descriptions' ).addClass( 'is-center' );
        } else {
            $( '.num-descriptions' ).removeClass( 'is-center' );
            $( '.num-descriptions' ).addClass( 'is-right' );
        }

        $( '.num-desc-entry.active' ).removeClass( 'active' );
        $( '.numbers .num.active' ).removeClass( 'active' );
        
        $( this ).addClass( 'active' );
        $( '.num-desc-entry[data-num=' + theNum + ']' ).addClass( 'active' );

    } );

    $( '.filter-block-header' ).click( function() {
        $( this ).parents( '.filter-block' ).toggleClass( 'active' );
    } );

    $( '.filter-entry' ).click( function() {
        $( this ).toggleClass( 'active' );
        filterEntries();

    } );

    $( '.details-menu-entry' ).click( function() {
        if ( $( this ).hasClass( 'active' ) ) {
            return false;
        }
        const theIndex = $( this ).index();
        $( '.details-menu-entry.active' ).removeClass( 'active' );
        $( '.details-data-entry.active' ).removeClass( 'active' );
        
        $( this ).addClass( 'active' );
        $( '.details-data-entry' ).eq( theIndex ).addClass( 'active' );

    } );

    $( '#reserve-car' ).click( function() {
        $( '#reserve-car-overlay' ).addClass( 'active' );
    } );

    $( '#more-info' ).click( function() {
        $( '#more-info-overlay' ).addClass( 'active' );
    } );

    $( '#to-pdf' ).click( function() {
        window.print();
    } );

    $( '.close-overlay' ).click( function() {
        $( this ).parents( '.overlay-wrapper' ).removeClass( 'active' );
    } );

    $( '.menu-toggler' ).click( function() {
        $( '.site-header nav' ).toggleClass( 'active' );
    } );

    $( '.message-icon' ).click( function() {
        $( '#contact-overlay' ).addClass( 'active' );
    } );

    $( '.filter-header .icon' ).click( function() {
        $( '.filters' ).toggleClass( 'active' );
    } );

    function filterEntries() {
        const filterObj = setupFilterObj();

        const postType = $( '.sidebar.filters' ).attr( 'data-cartype' ) === 'used' ? 'used_car' : 'car';
        //console.log( postType );

        console.log( filterObj );
        
        $.ajax( {
            type: 'post',
            url: ajaxLocal.ajaxUrl,
            data: {
                action: 'filter_cars',
                filters: filterObj,
                post_type: postType,
            },
            success: ( response ) => {
                console.log( response );
                $( '.filter-results' ).html( response );
            },
            error: ( e ) => {
                console.log( e );
            },
        } );
    }

    function setupFilterObj() {

        const filterObj = {};
        $( '.filter-body' ).each( function() {
            const activeFilters = $( this ).find( '.filter-entry.active' );
            if ( activeFilters.length ) {
                filterObj[ $( this ).data( 'type' ) ] = [];
                const activeFilter = filterObj[ $( this ).data( 'type' ) ];
                activeFilters.each( function() {
                    const filterId = $( this ).data( 'id' );
                    activeFilter.push( filterId );
                } );
            }
        } );

        const baseMinPrice = $( '#price-slide' ).data( 'min' ) ? $( '#price-slide' ).data( 'min' ) : 0;
        const baseMaxPrice = $( '#price-slide' ).data( 'max' ) ? $( '#price-slide' ).data( 'max' ) : 9999;

        if ( baseMinPrice !== minPriceVal || baseMaxPrice !== maxPriceVal ) {
            filterObj.price = {};
            filterObj.price.min = minPriceVal;
            filterObj.price.max = maxPriceVal;
        }

        return filterObj;
    }

    new Swiper( '.available-car-swiper', {
        // Optional parameters
        slidesPerView: 1.3,
        spaceBetween: 20,
        // If we need pagination
        breakpoints: {

            560: {
                slidesPerView: 2.5,
                spaceBetween: 40,
            },
            768: {
                slidesPerView: 3.5,
                spaceBetween: 60,
            },
        },
        pagination: {
            el: '.swiper-pagination',
        },
        // Navigation arrows
        navigation: {
            nextEl: '.available-car-swiper-wrapper .arrow-right',
            prevEl: '.available-car-swiper-wrapper .arrow-left',
        },
    } );

    new Swiper( '.review-swiper', {
        // Optional parameters
        slidesPerView: 1,
        spaceBetween: 35,
        // If we need pagination
        breakpoints: {

            560: {
                slidesPerView: 2,
                spaceBetween: 55,
            },
            1024: {
                slidesPerView: 3,
                spaceBetween: 85,
            },
        },
        pagination: {
            el: '.swiper-pagination',
        },
        // Navigation arrows
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
    } );

    const mainGalSwiper = new Swiper( '.main-image', {
        // Optional parameters
        slidesPerView: 1,
        spaceBetween: 85,
        // If we need pagination
        pagination: {
            el: '.swiper-pagination',
        },
        // Navigation arrows
        navigation: {
            nextEl: '.arrow-right',
            prevEl: '.arrow-left',
        },
        on: {
            slideChangeTransitionEnd: ( e ) => {
                const theSlide = $( '.main-image .slide' ).eq( e.activeIndex ).data( 'id' );
                const thumbSlide = $( '.gal-entry[data-id="' + theSlide + '"]' ).parents( '.gal-swiper-slide' ).index();
                $( '.gal-entry.active' ).removeClass( 'active' );
                $( '.gal-entry[data-id="' + theSlide + '"]' ).addClass( 'active' );
                galSwiper.slideTo( thumbSlide );
            },
        },
    } );
    
    const galSwiper = new Swiper( '.gallery-thumbnails', {
        // Optional parameters
        slidesPerView: 4,
        spaceBetween: 10,
        direction: 'vertical',
        /* grid: {
            rows: 2,
        }, */
        // Navigation arrows
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
    } );

    $( '.gal-entry' ).click( function() {
        const currentID = $( this ).data( 'id' );
        const theIndex = $( '.main-image .slide[data-id="' + currentID + '"]' ).index();
        mainGalSwiper.slideTo( theIndex );
    } );

    if ( $( '.single.single-car' ).length || $( '.single.single-used_car' ).length ) {
        setTimeout( () => { 
            const field = $( '#car-name' );
            const fieldType = $( '#car-type' );
            const websiteUrl = $( '#website-url' );
            const title = $( 'h1.car-title' ).text();
            
            field.val( title );
            fieldType.val( $( '.single.single-car' ).length ? 'new' : 'used' );
            websiteUrl.val( window.location.href );
        }, 1000 );
    }
} );
