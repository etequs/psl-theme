<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package psl
 */

?>

    <?php
        $riga_phones = $args['conf']['riga_phone_number'] ? explode('|', $args['conf']['riga_phone_number']) : null;
        $valmiera_phones = $args['conf']['valmiera_phone_number'] ? explode('|', $args['conf']['valmiera_phone_number']) : null;

        $riga_emails = $args['conf']['email'] ? explode('|', $args['conf']['email']) : null;
        $valmiera_emails = $args['conf']['valmiera_email'] ? explode('|', $args['conf']['valmiera_email']) : null;
    ?>

	<footer class="site-footer">
        <div class="site-center">
            <div class="footer-col show-in-print footer-col-1">
            <?php wp_nav_menu( array( 'theme_location' => 'footer-menu-left' ) ); ?>
            </div>
            <div class="footer-col footer-col-2">
                <p><a href="<?php echo get_site_url(); ?>"><?php echo $args["txt"]['main_page']; ?></a></p>
                <?php wp_nav_menu( array( 'theme_location' => 'footer-menu' ) ); ?>
            </div>
            <div class="footer-col show-in-print footer-col-3">
                <div class="cont-block">
                    <p class="footer-block-title"><?php echo $args["txt"]['riga_label']; ?></p>
                    <?php 
                    foreach ( $riga_phones as $phone ) echo sprintf('<p class="footer-cont-entry"><a href="tel:%s">%s</a></p>', $phone, $phone);
                    foreach ( $riga_emails as $mail ) echo sprintf('<p class="footer-cont-entry"><a href="mailto:%s">%s</a></p>', $mail, $mail);
                    ?>
                </div>
               
                <div class="social-links">
                    <div class="social-link">
                        <?php echo get_icon('instagram', $args['icons'] ); ?>
                        <a href="<?php echo $args['conf']["social_network_links"]["instagram"]?>"></a>
                    </div>
                    <div class="social-link">
                        <?php echo get_icon('facebook', $args['icons'] ); ?>
                        <a href="<?php echo $args['conf']["social_network_links"]["facebook"]?>"></a>
                    </div>
                    <div class="social-link">
                        <?php echo get_icon('youtube', $args['icons'] ); ?>
                        <a href="<?php echo $args['conf']["social_network_links"]["youtube"]?>"></a>
                    </div>
                    <div class="social-link">
                        <?php echo get_icon('whatsapp', $args['icons'] ); ?>
                        <a href="<?php echo $args['conf']["social_network_links"]["whatsapp"]?>"></a>
                    </div>
                </div>
            </div>
        </div>
	</footer><!-- #colophon -->
    <div class="copyright"><?php echo $args["txt"]['copyright']; ?></div>
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
