<?php /* Template Name: Contacts */ ?>

<?php 

get_header( null, array('icons' => $icons, 'conf' => $conf['data']) );

?>

    <main class="site-main page-layout formatted-text contacts-page">

        <div class="site-center">
            <div class="breadcrumbs"><?php echo generate_breadcrumbs(); ?></div>

            <?php
            while ( have_posts() ) :
                the_post(); 
            ?>

                

                <h2><?php echo $conf['txt']['about_us_heading']; ?></h2>
                <div class="about-us-wrapper">
                    <div class="about-us-content">
                        <?php echo apply_filters('the_content', $post->post_content); ?>
                    </div>
                    <div class="about-us-img">
                        <img src="<?php echo get_the_post_thumbnail_url(); ?>">
                    </div>
                </div>

                
                <div class="cont-map">
                    <?php echo $conf['data']['map_link']; ?>
                </div>

                <div class="cols">
                    <div class="left-block">
                        <h2><?php echo $post->post_title; ?></h2>

                        <p class="before-info"><?php echo $conf['data']['main_address'];?></p>
                        <div class="col-data-wrapper">
                            <div class="phones">
                                <div class="icon-text before-info"><?php echo get_icon('mail', $icons ); ?><p><?php echo $conf['data']['email'];?></p></div>
                                <div class="info-block">
                                    <h3><?php echo $conf['txt']['сar_showroom_label']; ?></h3>
                                    <div class="icon-text"><?php echo get_icon('phone', $icons ); ?><p><?php echo $conf['data']['main_phone']; ?></p></div>
                                </div>
                                <div class="info-block">
                                    <h3><?php echo $conf['txt']['car_service_label']; ?></h3>
                                    <div class="icon-text"><?php echo get_icon('phone', $icons ); ?><p><?php echo $conf['data']['riga_service_phone']; ?></p></div>
                                </div>
                            </div>
                            <div class="work-hours">
                                <h3><?php echo $conf['txt']['working_hours_label']; ?></h3>
                                <p><?php echo apply_filters('the_content', $conf['data']['working_hours']); ?></p>
                            </div>
                        </div>
                    </div>
                    <div class="right-block">
                        <?php

                        $options_page = 47;


                        $service_page_id = get_field('service_page', $options_page);
                        $contact_data = get_field('service_info', $service_page_id);

                        ?>

                            <h2><?php echo $contact_data['title']; ?></h2>
                            <p class="before-info"><?php echo $contact_data['address'];?></p>
                            

                            <div class="col-data-wrapper">
                                <div class="phones">
                                    <div class="icon-text before-info"><?php echo get_icon('mail', $icons ); ?><p><?php echo $contact_data['email'];?></p></div>
                                    <div class="info-block">
                                        <h3><?php echo $conf['txt']['car_service_label']; ?></h3>
                                        <div class="icon-text"><?php echo get_icon('phone', $icons ); ?><p><?php echo $contact_data['service_phones']; ?></p></div>
                                    </div>
                                    <div class="info-block">
                                        <h3><?php echo $conf['txt']['reserve_parts_label']; ?></h3>
                                        <div class="icon-text"><?php echo get_icon('phone', $icons ); ?><p><?php echo $contact_data['reserve_parts_phones']; ?></p></div>
                                    </div>
                                </div>
                                <div class="work-hours">
                                    <h3><?php echo $conf['txt']['working_hours_label']; ?></h3>
                                    <p><?php echo apply_filters('the_content', $contact_data['working_hours']); ?></p>
                                </div>
                            </div>
                    </div>

                </div>

                

                <h2><?php echo $conf['txt']['send_message_heading']; ?></h2>
                <div class="contact-form-wrapper">
                    <?php echo do_shortcode( '[contact-form-7 id="108" title="Contact page form"]' ); ?>
                </div>

                
            <?php
            endwhile; // End of the loop.
            ?>
        </div>

	</main><!-- #main -->

<?php
get_footer( null, array('icons' => $icons, 'conf' => $conf['data'], 'txt' => $conf['txt'] ));
