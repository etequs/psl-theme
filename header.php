<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package psl
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<?php wp_head(); ?>

    <!-- Font import start -->

    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Raleway:wght@400;700&display=swap" rel="stylesheet">
    <style>@import url('https://fonts.googleapis.com/css2?family=Raleway:wght@400;700&display=swap');</style>

    <!-- Font import end -->
</head>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>
<div id="page" class="site">
	<header class="site-header">
        <div class="site-center">
            <div class="logo">
                <a href="<?php echo get_site_url(); ?>"></a>
            </div>

            <nav>
                <?php wp_nav_menu( array( 'theme_location' => 'main-menu' ) ); ?>
                <div class="phone">
                    <?php echo get_icon('phone', $args['icons'] ); ?>
                    <?php
                        $phone = $post->ID !== 1209 ? $args['conf']['main_phone'] : $args['conf']['riga_service_phone'];
                    ?>
                    <span class="phone-num"><a href="tel:<?php echo $phone; ?>"><?php echo $phone; ?></a></span>
                </div>
            </nav>
            <div class="menu-toggler">
                <?php
                    echo get_icon('menu', $args['icons']);
                ?>
            </div>
        </div>
    </header>
