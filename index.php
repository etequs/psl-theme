<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package psl
 */

get_header( null, array('icons' => $icons, 'conf' => $conf['data'] ) );



get_template_part( 'template-parts/index/large_banner');
get_template_part( 'template-parts/index/advantages', null, array("txt" => $conf['txt']) );
get_template_part( 'template-parts/index/available_cars', null, array("txt" => $conf['txt']) );
get_template_part( 'template-parts/index/about_psl', null, array("txt" => $conf['txt']) );
get_template_part( 'template-parts/index/client_reviews', null, array("txt" => $conf['txt']) );
?>
<div class="message-icon">
    <?php echo get_icon('msg', $icons ); ?>
</div>

<?php
get_template_part( 'template-parts/overlays/overlay-container', null, array("icons" => $icons, 'id' => 'contact-overlay', 'type' => 'form', 'shortcode' => '[contact-form-7 id="132" title="Send message"]') );
get_footer( null, array('icons' => $icons, 'conf' => $conf['data'], 'txt' => $conf['txt'] ));
