<?php /* Template Name: News */ ?>

<?php 

get_header( null, array('icons' => $icons, 'conf' => $conf['data']) );

?>

<?php

    $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;

    $args3 = array(
        "post_status"       =>  "publish",
        "posts_per_page"    =>  -1,
        "post_type"         => "post",
        "posts_per_page" => 10, // number of posts to display per page
        "paged" => $paged
    );

    $all_news_posts = new WP_Query($args3);
    //var_dump($all_news_posts);
?>

    <main class="site-main page-layout">

        <div class="site-center">
            <div class="breadcrumbs"><?php echo generate_breadcrumbs(); ?></div>

            <h1><?php echo $conf['txt']['news_page_heading']; ?></h1>

            <?php 
                if ($all_news_posts->have_posts()) :
                    while( $all_news_posts->have_posts() ) :
                        $all_news_posts->the_post();
                        $postlink = get_permalink( $post->ID );
                        
                            ?>
                            <div class="post-entry">
                                <div class="entry-image">
                                    <img src="<?php echo get_the_post_thumbnail_url( $post->ID ); ?>" alt="">
                                    <a href="<?php echo $postlink; ?>"></a>
                                </div>
                                <div class="entry-description">
                                    <div class="entry-title"><a href="<?php echo $postlink; ?>"><?php echo $post->post_title; ?></a></div>
                                    <div class="entry-short-details">
                                        <?php echo get_the_excerpt($post->ID); ?>
                                        <span class="read-more"><a href="<?php echo $postlink; ?>"><?php echo $conf['txt']['read_more_label']; ?></a></span>
                                    </div>
                                    <div class="entry-date"><?php echo get_the_date('d.m.Y', $post->ID);?></div>
                                </div>
                            </div>
                        <?php endwhile; ?>

                <?php endif; ?>
                <?php echo "<div class='page-nav-container'>" . paginate_links(array(
                    'total' => $all_news_posts ->max_num_pages,
                    'prev_text' => __('<'),
                    'next_text' => __('>')
                )) . "</div>";
                
                ?>
            </div>
        </div>
	</main><!-- #main -->

<?php
get_footer( null, array('icons' => $icons, 'conf' => $conf['data'], 'txt' => $conf['txt'] ));
