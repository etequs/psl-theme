<?php /* Template Name: Car list */ ?>

<?php 

get_header( null, array('icons' => $icons, 'conf' => $conf['data'], 'txt' => $conf['txt'] ) );

get_template_part( 'template-parts/car_list/layout', null, array('icons' => $icons, "car_type" => "new", 'txt' => $conf['txt']) );

get_footer( null, array('icons' => $icons, 'conf' => $conf['data'], 'txt' => $conf['txt'] ));
