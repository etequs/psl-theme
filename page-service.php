<?php /* Template Name: Service */ ?>

<?php 
get_header( null, array('icons' => $icons, 'conf' => $conf['data']) );
$form = get_field('contact_form');
?>

<?php get_template_part( 'template-parts/service/top_image' ); ?>
<?php get_template_part( 'template-parts/service/offer_list' ); ?>


<?php get_template_part( 'template-parts/service/advantages' ); ?>


<?php
    $content = get_field('additional_tasks');
    if ($content) :
        $content = apply_filters( 'the_content', $content );
        echo '<div class="additional-price-list formatted-content">'.$content.'</div>';
    endif;
?>

<div class="service-form site-center" id="<?php echo $form['contact_form_id']; ?>">
    <h2><?php echo $form['title'];?></h2>
    <p class="subtitle"><?php echo $form['subtitle']; ?></p>
    <div class="contact-form-wrapper">
        <?php echo do_shortcode( $form['form_shortcode'] ); ?>
    </div>
</div>

<?php get_template_part( 'template-parts/service/contacts', null, array( 'icons' => $icons ) ); ?>

<?php
get_footer( null, array('icons' => $icons, 'conf' => $conf['data'], 'txt' => $conf['txt'] ));
