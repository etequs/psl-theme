<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package psl
 */

get_header( null, array('icons' => $icons, 'conf' => $conf['data']) );

//$title = $post->post_type == 'post' ? __('Jaunumi', 'psl') : $post->post_title;
$title = $post->post_title;

?>

    <main class="site-main page-layout">

        <div class="site-center">
            <div class="breadcrumbs"><?php echo generate_breadcrumbs(); ?></div>
            <h1><?php echo $title; ?> </h1>
            <?php
            while ( have_posts() ) :
                the_post();?>

                <div class="single-post-content flex-single-post">
                    <div class="single-post-image">
                        <img src="<?php echo get_the_post_thumbnail_url( $post->ID ); ?>" alt="">
                    </div>
                    <div class="single-post-content formatted-content">
                        <?php echo apply_filters('the_content', $post->post_content); ?>
                    </div>
                </div>
                

            <?php 
            endwhile; // End of the loop.
            ?>
        </div>

	</main><!-- #main -->

<?php
get_footer( null, array('icons' => $icons, 'conf' => $conf['data'], 'txt' => $conf['txt'] ));
